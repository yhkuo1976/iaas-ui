

function showMessage(success) {
    var message = sessionStorage.getItem("message")||"";
    var messageBox = document.getElementById("message-box");
    if (success) {
        var html = '';
        html += `
            <div class="alert alert-dismissible alert-success" id="message">
                <button type="button" class="close" id="close-button" data-dismiss="alert">
                ×
                </button>
                <strong class="text-break text-wrap">${message}</strong>
            </div>
        `;
        messageBox.innerHTML += html;
    } else {
        var html = '';
        html += `
            <div class="alert alert-dismissible alert-danger" id="message">
                <button type="button" class="close" id="close-button" data-dismiss="alert">
                ×
                </button>
                <strong class="text-break text-wrap">${message}</strong>
            </div>
        `;
        messageBox.innerHTML += html;
    }
    setTimeout(function() {
        var addedMessageBox = document.getElementById("message");
        if (addedMessageBox) {
            addedMessageBox.remove();
        }
    }, 4000);
    clearSessionMessage();
}


window.addEventListener('load', () => {
    var success = sessionStorage.getItem("success")||"";
    if (success == "true") {
        showMessage(true);
    } else if (success == "false") {
        showMessage(false);
    }
});


function clearSessionMessage () {
    sessionStorage.removeItem("success");
    sessionStorage.removeItem("message");
}


document.addEventListener("DOMContentLoaded", function() {
    var messageBox = document.getElementById("django-message");
    if (messageBox) {
        setTimeout(function() {
            if (messageBox) {
                messageBox.remove();
            }
        }, 4000);
    }
});