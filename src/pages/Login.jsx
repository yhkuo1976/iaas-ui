import React, { useEffect }  from 'react';

const LoginPage = () => {
    useEffect(() => {
        const particlesMinScript = document.createElement('script');
        particlesMinScript.src = '/assets/plugins/particles-js/particles.min.js';

        document.body.appendChild(particlesMinScript);

        return () => {
          document.body.removeChild(particlesMinScript);
        };
    }, []);
    return (
        <div className="authentication">
            <div className="container">
                <div className="col-md-12 content-center">
                <div className="row clearfix">
                    <div className="col-lg-5 col-md-12 offset-lg-1">
                        <div className="card-plain">
                            <div className="header">
                                <h5 className="pt-4">Log in</h5>
                            </div>                       
                                <div className="input-group">
                                    <input type="text" id="username" name="username" className="form-control" placeholder="User name" required></input>
                                    <span className="input-group-addon"><i className="zmdi zmdi-account-circle"></i></span>
                                </div>
                                <div className="input-group">
                                    <input type="password" id="password" name="password" className="form-control" placeholder="Password" required></input>
                                    <span className="input-group-addon"><i className="zmdi zmdi-lock"></i></span>
                                </div>

                                <div className="footer pt-5 pb-4">
                                    {/* <spam className="col-red">{{message}}</spam> */}
                                    <div className="col pb-2 ">
                                    <button type="submit" className="btn btn-primary btn-round btn-block p-1"><h5 className="m-0">Sign in</h5></button>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-12">
                        <div className="company_detail">
                            <h4 className="logo">
                                <img className="loginLogoSize" src="assets/images/apps-logo.png" alt=""></img>
                            </h4>
                            <h3 className="text-capitalize noLetterSpacing">IaaS Management System</h3>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div id="particles-js"></div>
        </div>
  );
};

export default LoginPage;
