import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import LoginPage from './pages/Login';

function App() {
  return (
    <Router>
      <div>
      <Routes>
          <Route path="/" exact component={Home} />
          <Route path="/login" component={LoginPage} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;